# 0 - SQLLite To API

API Generierung aus SQL Schema und gegebener SQLLite Datenbank

## sandman2 installieren und API erstellen

```
#!/bin/bash
pip install sandman2 --user
pip install sqlalchemy --upgrade --user
set PATH=%PATH%;C:\Users\Eric\AppData\Roaming\Python\Python38\Scripts
# open the api
sandman2ctl sqlite+pysqlite:///C:\Users\Eric\sqlite2api\car_company_database\Car_Database.db -p <port>
```

